
// MFCPlayerAppDlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CMFCPlayerAppDlg 对话框
class CMFCPlayerAppDlg : public CDialogEx
{
// 构造
public:
	CMFCPlayerAppDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_MFCPLAYERAPP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
    CWinThread *pThreadWorkPlay;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnEnChangeEdit1();
    afx_msg void OnEnChangeEditFilePath();
    CEdit m_edit_file_path;
    afx_msg void OnBnClickedButtonFileBroswer();
    afx_msg void OnBnClickedButtonClose();
    afx_msg void OnBnClickedButtonAbout();
    afx_msg void OnBnClickedButtonPlay();
    afx_msg void OnBnClickedButtonStop();
    afx_msg void OnStnClickedVideoWin();
    afx_msg void OnBnClickedButtonPause();
    afx_msg void OnStnClickedWin();
};
