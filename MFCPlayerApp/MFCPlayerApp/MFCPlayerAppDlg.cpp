
// MFCPlayerAppDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFCPlayerApp.h"
#include "MFCPlayerAppDlg.h"
#include "afxdialogex.h"

#include "src/codec/MediaAVCodec.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCPlayerAppDlg 对话框



CMFCPlayerAppDlg::CMFCPlayerAppDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCPlayerAppDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCPlayerAppDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT_FILE_PATH, m_edit_file_path);
}

BEGIN_MESSAGE_MAP(CMFCPlayerAppDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_EN_CHANGE(IDC_EDIT_FILE_PATH, &CMFCPlayerAppDlg::OnEnChangeEditFilePath)
    ON_BN_CLICKED(IDC_BUTTON_FILE_BROSWER, &CMFCPlayerAppDlg::OnBnClickedButtonFileBroswer)
    ON_BN_CLICKED(ID_BUTTON_CLOSE, &CMFCPlayerAppDlg::OnBnClickedButtonClose)
    ON_BN_CLICKED(ID_BUTTON_ABOUT, &CMFCPlayerAppDlg::OnBnClickedButtonAbout)
    ON_BN_CLICKED(IDC_BUTTON_PLAY, &CMFCPlayerAppDlg::OnBnClickedButtonPlay)
    ON_BN_CLICKED(IDC_BUTTON_STOP, &CMFCPlayerAppDlg::OnBnClickedButtonStop)
    ON_BN_CLICKED(IDC_BUTTON_PAUSE, &CMFCPlayerAppDlg::OnBnClickedButtonPause)
    ON_STN_CLICKED(IDC_WIN, &CMFCPlayerAppDlg::OnStnClickedWin)
END_MESSAGE_MAP()


// CMFCPlayerAppDlg 消息处理程序

BOOL CMFCPlayerAppDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCPlayerAppDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCPlayerAppDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCPlayerAppDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCPlayerAppDlg::OnEnChangeEdit1()
{
    // TODO:  如果该控件是 RICHEDIT 控件，它将不
    // 发送此通知，除非重写 CDialogEx::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。

    // TODO:  在此添加控件通知处理程序代码
}


void CMFCPlayerAppDlg::OnEnChangeEditFilePath()
{
    // TODO:  如果该控件是 RICHEDIT 控件，它将不
    // 发送此通知，除非重写 CDialogEx::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。

    // TODO:  在此添加控件通知处理程序代码
}


void CMFCPlayerAppDlg::OnBnClickedButtonFileBroswer()
{
    // TODO:  在此添加控件通知处理程序代码

    CString filePathName;
    CFileDialog dlg(TRUE, NULL, NULL, NULL, NULL);///TRUE为OPEN对话框，FALSE为SAVE AS对话框 
    if (dlg.DoModal() == IDOK) {
        filePathName = dlg.GetPathName();
        m_edit_file_path.SetWindowText(filePathName);
    }
}


void CMFCPlayerAppDlg::OnBnClickedButtonClose()
{
    // TODO:  在此添加控件通知处理程序代码
    //close the play video screen
    this->GetDlgItem(IDC_WIN)->ShowWindow(SW_SHOWNORMAL);
    CDialogEx::OnCancel();
    media_codec_fini();

    

}


void CMFCPlayerAppDlg::OnBnClickedButtonAbout()
{
    // TODO:  在此添加控件通知处理程序代码
    CAboutDlg aboutDlg;
    aboutDlg.DoModal();
}

/*
http://blog.csdn.net/sd2131512/article/details/6601228
创建工作者线程指针函数
*/
UINT WorkPlay_Thread(LPVOID lpParam){
    CString strPath;
    CMFCPlayerAppDlg *dlg = (CMFCPlayerAppDlg *)lpParam;
    dlg->m_edit_file_path.GetWindowTextA(strPath);
    char* strFilePath = (LPSTR)(LPCSTR)strPath;
   // if (media_codec_init((const char*)strFilePath, NULL) == 0)
    if (media_codec_init((const char*)strFilePath, dlg->GetDlgItem(IDC_WIN)->GetSafeHwnd()) == 0)
    {
        media_codec_play();
    }

    return 0;
}

void CMFCPlayerAppDlg::OnBnClickedButtonPlay()
{
    // TODO:  在此添加控件通知处理程序代码
    if (media_codec_isInit())
    {
        media_codec_play();
    }
    else
    {
        pThreadWorkPlay = AfxBeginThread(WorkPlay_Thread, this);//开启线程
    }
    

}


void CMFCPlayerAppDlg::OnBnClickedButtonStop()
{
    // TODO:  在此添加控件通知处理程序代码
    media_codec_stop();
}


void CMFCPlayerAppDlg::OnStnClickedVideoWin()
{
    // TODO:  在此添加控件通知处理程序代码
}


void CMFCPlayerAppDlg::OnBnClickedButtonPause()
{
    // TODO:  在此添加控件通知处理程序代码
    media_codec_pause();
}


void CMFCPlayerAppDlg::OnStnClickedWin()
{
    // TODO:  在此添加控件通知处理程序代码
}
