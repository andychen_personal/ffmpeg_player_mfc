//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 MFCPlayerApp.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MFCPLAYERAPP_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDB_BITMAP2                     131
#define IDR_MENU1                       132
#define IDC_EDIT_FILE_PATH              1000
#define IDC_BUTTON_FILE_BROSWER         1001
#define IDC_BUTTON_PLAY                 1002
#define IDC_BUTTON_PAUSE                1003
#define IDC_BUTTON_STOP                 1004
#define ID_BUTTON_CLOSE                 1005
#define ID_BUTTON_ABOUT                 1006
#define IDC_MFCBUTTON1                  1008
#define IDC_VIDEO_WIN                   1009
#define IDC_WIN                         1010
#define ID_HELP_ABOUT                   32771
#define ID_CONTROL_PLAY                 32772
#define ID_CONTROL_PAUSE                32773
#define ID_CONTROL_STOP                 32774
#define ID_FILE_OPEN32775               32775
#define ID_FILE_EXIT                    32776
#define ID_BUTTON_PLAY                  32777
#define ID_BUTTON_PAUSE                 32778
#define ID_BUTTON_STOP                  32779
#define IDC_BUTTON_ABOUT                32780
#define IDC_BUTTON_CLOSE                32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
