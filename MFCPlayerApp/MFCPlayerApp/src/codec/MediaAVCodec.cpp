#include "stdafx.h"
#include "stdlib.h"
#include "MediaAVCodec.h"
#include "AudioDecoder.h"
#include "VideoDecoder.h"

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"  
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <sdl2/SDL.h>
#ifdef __cplusplus
};
#endif
#endif

static AVFormatContext *pFormatCtx = NULL;
static AVPacket *m_pAVPacket;//avpkt The input AVPacket containing the input buffer.

static bool m_isStart = false;
static int m_decoder_pause;
static int m_decoder_flag;


static VideoDecoder m_videoDec;
//for Audio
static AudioDecoder m_audioDec;
static MediaCodecParams* pMediaRenderParams = NULL;



int media_codec_init(const char *filePath, const void* pRenderWindowFrom)
{
    
    int videoTypeIndex = -1;
    int audioTypeIndex = -1;
    unsigned int i = 0;

    if (m_isStart) return -1;


    // step0: register
    av_register_all();

    avformat_network_init();

    //step1: get the avformat context;
    pFormatCtx = avformat_alloc_context();

    if (filePath == NULL)
    {
        filePath = FILE_PATH_VIDEO;
    }
    printf("preare for OPEN file %s \n",filePath);
    //step2: open a file 
    if (avformat_open_input(&pFormatCtx, filePath, NULL, NULL) != 0)
    {
        AfxMessageBox("Couldn't open input stream.\n");
        printf("OPEN file failed \n");
        return -1;
    }
    //step3: find the stream info
    if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
    {
        AfxMessageBox("Couldn't find input stream.\n");
        printf("find file stream failed");
        return -1;
    }
    //step4: get the stream type
    for (i = 0; i < pFormatCtx->nb_streams; i++)
    {
        if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoTypeIndex = i;
            continue;
        }
        if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            audioTypeIndex = i;
            continue;
        }
        if (videoTypeIndex != -1 && audioTypeIndex != -1)
        {
            break;
        }
    }
    /*
    if (videoTypeIndex == -1 && audioTypeIndex == -1)
    {
        printf("can't find video audio stream return");
        return -1;
    }*/


    //step7: malloc the save data buffer
    m_pAVPacket = (AVPacket*)av_malloc(sizeof(AVPacket));

    pMediaRenderParams = (MediaCodecParams*)malloc(sizeof(MediaCodecParams));

    if (videoTypeIndex != -1)
    {
        pMediaRenderParams->pVidioInStream = pFormatCtx->streams[videoTypeIndex];
        pMediaRenderParams->pAvInputPacketData = m_pAVPacket;
        pMediaRenderParams->videoStreamIndex = videoTypeIndex;
        pMediaRenderParams->pWindowFrom = (void*)pRenderWindowFrom;
        m_videoDec.video_init(pMediaRenderParams);

    }


    if (audioTypeIndex != -1)
    {
        pMediaRenderParams->pAudioInStream = pFormatCtx->streams[audioTypeIndex];
        pMediaRenderParams->pAvInputPacketData = m_pAVPacket;
        pMediaRenderParams->audioStreamIndex = audioTypeIndex;

        m_audioDec.audio_init(pMediaRenderParams);
    }

    m_isStart = TRUE;
    return 0;


}

int media_codec_isInit()
{
    return (int)m_isStart;
}

static int m_media_codec_decoder()
{
  
    if (m_isStart == false) return -1;

    //step8 read frame 
    while (true)
    {
        if (m_decoder_pause)
        {
            continue;
        }
        if (m_decoder_flag == 0)
        {
            return -1;
        }
        if (pFormatCtx == NULL || m_pAVPacket == NULL) return -1;
        if (av_read_frame(pFormatCtx, m_pAVPacket) < 0)
        {
            return -1;
        }
        //get type stream data
        if (m_pAVPacket->stream_index == pMediaRenderParams->videoStreamIndex)
        {
            m_videoDec.video_decoder();

        }
         
        if (m_pAVPacket->stream_index == pMediaRenderParams->audioStreamIndex)
        {
            //FOR AUDIO
         //   m_audioDec.audio_decoder(1);
           m_audioDec.m_decoder();
        }
        else{
            m_audioDec.audio_decoder(0);
        }

        av_free_packet(m_pAVPacket);
    }

    return 0;

}


int media_codec_pause()
{
    if (m_decoder_flag)
    {
        m_decoder_pause = 1;
    }
    
    return 0;
}
int media_codec_stop()
{

    m_decoder_flag = 0;

    return 0;
}

int media_codec_play()
{
    if (m_decoder_flag == 0)
    {
        m_decoder_flag = 1;
        m_media_codec_decoder();
        
    } 
    if (m_decoder_pause == 1) {
        m_decoder_pause = 0;
    }
    return 0;
}


void media_codec_fini()
{
    printf("decoder fini \n");

    m_audioDec.audio_fini();


    if (m_pAVPacket != NULL)
    {
        av_free_packet(m_pAVPacket);
        m_pAVPacket = NULL;
    }
   
    
    avformat_close_input(&pFormatCtx);
    
    if (pMediaRenderParams != NULL)
    {
        free(pMediaRenderParams);
        pMediaRenderParams = NULL;
    }
  
    m_isStart = false;
    
}






