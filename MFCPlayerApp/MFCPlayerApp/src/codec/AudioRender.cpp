#include "stdafx.h"
#include "AudioRender.h"


AudioRender::AudioRender()
{
}


AudioRender::~AudioRender()
{
}


//Buffer:  
//|-----------|-------------|  
//chunk-------pos---len-----|  
static  Uint8  *audio_chunk;
static  Uint32  audio_len;
static  Uint8  *audio_pos;

/* The audio function callback takes the following parameters:
* stream: A pointer to the audio buffer to be filled
* len: The length (in bytes) of the audio buffer
* �ص�����
*/
static void  fill_audio_buffer(void *udata, Uint8 *stream, int len)
{
    //SDL 2.0
    SDL_memset(stream, 0, len);
    if (audio_len == 0)		/*  Only  play  if  we  have  data  left  */
        return;
    len = (len>audio_len ? audio_len : len);	/*  Mix  as  much  data  as  possible  */

    SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);
    audio_pos += len;
    audio_len -= len;
}
static int m_audio_thread_exit;
static int m_audio_thread_pause;
#define AUDIO_RENDER_REFRESH_EVENT  (SDL_USEREVENT + 3)
#define AUDIO_RENDER_EXIT_EVENT  (SDL_USEREVENT + 4)

static int m_AudioRenderEventsThread(void* params)
{
    while (!m_audio_thread_exit)
    {
        if (!m_audio_thread_pause)
        {
            SDL_Event events;
            events.type = AUDIO_RENDER_REFRESH_EVENT;
            SDL_PushEvent(&events);
        }

        SDL_Delay(15);
    }
    m_audio_thread_exit = true;
    //Break
    SDL_Event event;
    event.type = AUDIO_RENDER_EXIT_EVENT;
    SDL_PushEvent(&event);

    return 0;
}
int AudioRender::audio_render_init(MediaCodecParams* pMediaParams)
{
    //Init  
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
        printf("Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    //SDL_AudioSpec  
    SDL_AudioSpec wanted_spec;
    wanted_spec.freq = pMediaParams->out_sample_rate;
    wanted_spec.format = AUDIO_S16SYS;
    wanted_spec.channels = pMediaParams->out_channels;
    wanted_spec.silence = 0;
    wanted_spec.samples = pMediaParams->out_nb_samples;
    wanted_spec.callback = fill_audio_buffer;
    wanted_spec.userdata = pMediaParams->pCodecCtx;

    if (SDL_OpenAudio(&wanted_spec, NULL)<0){
        printf("can't open audio.\n");
        return -1;
    }

    pAudioRenderThread = SDL_CreateThread(m_AudioRenderEventsThread, NULL, NULL);
    return 0;
}

int AudioRender::audio_render(uint8_t *outBuffer, int bufferSize)
{
    //Set audio buffer (PCM data)  
    //loop in listener the sync event
    SDL_Event event;
    SDL_WaitEvent(&event);

    if (event.type == AUDIO_RENDER_REFRESH_EVENT)
    {

        SDL_PauseAudio(0);

        audio_chunk = (Uint8 *)outBuffer;
        //Audio buffer length  
        audio_len = bufferSize;

        audio_pos = audio_chunk;

        //  while (audio_len>0)//Wait until finish  
        //    SDL_Delay(1);

        
    }
    else if (event.type == SDL_QUIT)
    {
        m_audio_thread_exit = true;
    }
    else if (event.type == AUDIO_RENDER_EXIT_EVENT)
    {
        return -1;
    }
    return 0;
}
int AudioRender::audio_fini()
{
    //for audio
    SDL_CloseAudio();//Close SDL  
    return 0;
}
