/* FILE NAME: _MEDIA_AV_CODEC_.h
* Version: 1.0,  Date: xxxx-xx-xx
* Description: xxxxx
* Platform: xxxxx
*
***************************************************************************/
#ifndef  _MEDIA_AV_CODEC_
#define _MEDIA_AV_CODEC_

#include <stdio.h>
#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

//Output YUV420P data as a file 





#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio  
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "SDL2/SDL.h"
#ifdef __cplusplus
    //Output YUV420P data as a file 
#define OUTPUT_YUV420P 0
};
#endif
#endif

    int media_codec_init(const char *filePath, const void* pRenderWindowFrom);
    int media_codec_isInit();
    int media_codec_pause();
    int media_codec_stop();
    int media_codec_play();
    void media_codec_fini();



#endif // ! _MEDIA_AV_CODEC_