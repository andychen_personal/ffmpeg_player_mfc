#include "stdafx.h"
#include "AudioDecoder.h"
#include "AudioRender.h"
#include <Windows.h>

static AudioRender m_audioRender;

int index = 0;

static HANDLE m_handerThread;
static DWORD m_threadId;
static HANDLE hMutex;
int m_dec_status = 0;
static int m_audio_thread_status;

AudioDecoder::AudioDecoder()
{
}


AudioDecoder::~AudioDecoder()
{
}


int AudioDecoder::m_decoder()
{
    int ret = -1;
    int got_picture = 0;

    if (m_AudioDecInited == 0) return -1;

    /*
    if (av_read_frame(pFormatCtx, m_audioPacket) >= 0){
    if (m_audioPacket->stream_index == audioStream)
    {*/


    ret = avcodec_decode_audio4(pAudioCodecCtx, m_pAudioAVFrame, &got_picture, m_pAudioInputPacket);
    if (ret < 0) {
        printf("Error in decoding audio frame.\n");
        return -1;
    }
    if (got_picture > 0)
    {
        ret = swr_convert(m_pSwrCtx, &m_audio_out_buffer, MAX_AUDIO_FRAME_SIZE, (const uint8_t **)m_pAudioAVFrame->data, m_pAudioAVFrame->nb_samples);

        printf("index:%5d\t pts:%lld\t packet size:%d\n", index, m_pAudioInputPacket->pts, m_pAudioInputPacket->size);
#if OUTPUT_PCM  
        if (ret >= 0)
        {
            //Write PCM  
            fwrite(m_audio_out_buffer, 1, m_audio_out_buffer_size, m_pOutputPcmFile);
        }
#endif  
        index++;

    }
    m_audioRender.audio_render(m_audio_out_buffer, m_audio_out_buffer_size);
    return 0;

}

int AudioDecoder::audio_fini()
{
    m_audio_thread_status = 0;
    CloseHandle(m_handerThread);
#if OUTPUT_PCM  
    fclose(m_pOutputPcmFile);
#endif  

    //free audio
    if (m_pAudioAVFrame != NULL)
    {
        av_frame_free(&m_pAudioAVFrame);
        m_pAudioAVFrame = NULL;
    }
    //audio
    swr_free(&m_pSwrCtx);
    avcodec_close(pAudioCodecCtx);

    m_audioRender.audio_fini();
    m_AudioDecInited = 0;
    return 0;
}


DWORD WINAPI ThreadAudioProc(LPVOID lpParam)
{
    printf("sub thread started\n");
    m_audio_thread_status = 1;
    while (m_audio_thread_status)
    {
        if (m_dec_status == 0) continue;

        //获取互斥体，如果被其他线程获取了就一直等待下去  
        WaitForSingleObject(&hMutex, INFINITE);
        ((AudioDecoder*)lpParam)->m_decoder();

        //释放互斥体  
        ReleaseMutex(&hMutex);
    }
    
    CloseHandle(hMutex);
    

    printf("sub thread finished\n");
    return 0;
}

int AudioDecoder::audio_init(MediaCodecParams* pParams)
{
    if (pParams == NULL)
        return -1;
    
    m_pAudioInputPacket = (AVPacket*)pParams->pAvInputPacketData;
    if (m_pAudioInputPacket == NULL)
    {
        return -1;
    }
    AVStream *pAudioStream = (AVStream*)pParams->pAudioInStream;
    if (pAudioStream == NULL)
        return -1;

    pAudioCodecCtx = pAudioStream->codec;
    pAudioCodec = avcodec_find_decoder(pAudioCodecCtx->codec_id);
    m_audio_out_buffer = (uint8_t *)av_malloc(MAX_AUDIO_FRAME_SIZE * 2);
    m_pAudioAVFrame = av_frame_alloc();

    av_init_packet(m_pAudioInputPacket);


#if OUTPUT_PCM  
    DeleteFile(FILE_AUDIO_PCM_NAME);
    m_pOutputPcmFile = fopen(FILE_AUDIO_PCM_NAME, "wb+");
    
#endif  

    if (pAudioCodec == NULL)
    {
        return -1;
    }

    //step6: open audio decoder
    if (avcodec_open2(pAudioCodecCtx, pAudioCodec, NULL) != 0) {
        printf("can't open AUDIO decoder");
        return -1;
    }
    

    //Out Audio Param  
    uint64_t out_channel_layout = AV_CH_LAYOUT_STEREO;
    //AAC:1024  MP3:1152  
    int out_nb_samples = pAudioCodecCtx->frame_size;
    AVSampleFormat out_sample_fmt = AV_SAMPLE_FMT_S16;
    int out_sample_rate = 44100;
    int out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
    //Out Buffer Size  
    m_audio_out_buffer_size = av_samples_get_buffer_size(NULL, out_channels, out_nb_samples, out_sample_fmt, 1);

    if (pParams != NULL)
    {
        pParams->out_nb_samples = out_nb_samples;
        pParams->out_channels = out_channels;
        pParams->out_sample_rate = out_sample_rate;
        pParams->pCodecCtx = pAudioCodecCtx;
    }

    

    //FIX:Some Codec's Context Information is missing  
    int64_t in_channel_layout = av_get_default_channel_layout(pAudioCodecCtx->channels);
    

    m_pSwrCtx = swr_alloc();
    m_pSwrCtx = swr_alloc_set_opts(m_pSwrCtx, out_channel_layout, out_sample_fmt, out_sample_rate,
        in_channel_layout, pAudioCodecCtx->sample_fmt, pAudioCodecCtx->sample_rate, 0, NULL);
    swr_init(m_pSwrCtx);


    m_audioRender.audio_render_init(pParams);
    
    hMutex = CreateMutex(NULL, FALSE, TEXT("PlayMutex"));
    m_handerThread = CreateThread(NULL, 0, ThreadAudioProc, this, 0, &m_threadId);

    m_AudioDecInited = 1;
    index = 0;
    return 0;
}

int AudioDecoder::audio_decoder(int decStatus)
{
    m_dec_status = decStatus;
    return 0;
}
