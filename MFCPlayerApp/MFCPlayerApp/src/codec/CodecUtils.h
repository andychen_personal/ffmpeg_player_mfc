/* FILE NAME: _CODEC_UTILS_.h
* Version: 1.0,  Date: xxxx-xx-xx
* Description: xxxxx
* Platform: xxxxx
*
***************************************************************************/
#ifndef  _CODEC_UTILS_
#define _CODEC_UTILS_


#define OUTPUT_PCM 1
#define USE_SDL 1
#define FILE_AUDIO_PCM_NAME "output_ffmpeg.pcm"

#define OUTPUT_YUV420P 1
#define FILE_PATH_VIDEO "test_video_480x272.h264"  //"testEncode.h264"; 
#define FILE_PATH_VIDEO_OUTPUT_YUV420P "output_yuv420p.yuv"

#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio  


typedef  struct _MEDIA_CODEC_PARAMETERS_
{
    int width;
    int height;
    void* pWindowFrom;
    int out_sample_rate;
    int out_channels;
    int out_nb_samples;
    void* pCodecCtx;

    void* pAvInputPacketData;
    void* pVidioInStream;
    void* pAudioInStream;
    int videoStreamIndex;
    int audioStreamIndex;

}MediaCodecParams;

#endif