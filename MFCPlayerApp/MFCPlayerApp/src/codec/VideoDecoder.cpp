#include "stdafx.h"
#include "VideoDecoder.h"
#include "VideoSdlRender.h"
#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"  
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <sdl2/SDL.h>
#ifdef __cplusplus
};
#endif
#endif



static AVCodecContext *pCodecCtx = NULL;
static AVCodec *pCodec = NULL;
static AVFrame *pAVFrame = NULL, *pAVFrameYuv = NULL;
static uint8_t *m_out_buffer;
AVPacket *m_pInputPacket;//avpkt The input AVPacket containing the input buffer.

static SwsContext *pSwsContext;

FILE *pFileSaveYuv = NULL;
static VideoSdlRender m_SDLRender;


VideoDecoder::VideoDecoder()
{
}


VideoDecoder::~VideoDecoder()
{
}

int VideoDecoder::video_init(MediaCodecParams* pParams)
{
    if (pParams == NULL) return -1;

#ifdef OUTPUT_YUV420P 
    int errorno = fopen_s(&pFileSaveYuv, FILE_PATH_VIDEO_OUTPUT_YUV420P, "wb+");
    printf("OPEN file errorno %d \n", errorno);
#endif

    pAVFrame = av_frame_alloc();
    pAVFrameYuv = av_frame_alloc();

    m_pInputPacket = (AVPacket*)pParams->pAvInputPacketData;
    //step5: find VIDEO the decoder
    pCodecCtx = ((AVStream*)pParams->pVidioInStream)->codec;
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    m_out_buffer = (uint8_t *)av_malloc(avpicture_get_size(PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height));
    //for video
    pParams->width = pCodecCtx->width;
    pParams->height = pCodecCtx->height;

    //step6: open decoder
    if (avcodec_open2(pCodecCtx, pCodec, NULL) != 0) {
        printf("can't open decoder");
        return -1;
    }

    avpicture_fill((AVPicture *)pAVFrameYuv, m_out_buffer, PIX_FMT_YUV420P, pCodecCtx->width, pCodecCtx->height);

    //get the wscale context
    pSwsContext = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width,
        pCodecCtx->height, PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL);

    m_SDLRender.init(pParams);

    return 0;
}
int VideoDecoder::video_decoder()
{

    int intGotFrame = 0, ret = -1;
    int videoSize;
    int remaining = 0;


    //FOR VIDEO
    if (pCodec == NULL)
    {
        return -1;
    }

    //step9: todo decoder a frame
    ret = avcodec_decode_video2(pCodecCtx, pAVFrame, &intGotFrame, m_pInputPacket);
    if (ret < 0)
    {
        printf("decode video error return \n");
        return -1;
    }
    if (intGotFrame)
    {
        sws_scale(pSwsContext, pAVFrame->data, pAVFrame->linesize, 0, pCodecCtx->height, pAVFrameYuv->data, pAVFrameYuv->linesize);

        if (pFileSaveYuv)
        {
            //printf("output yuv data to local file BEGIN");
            //output the yuv data to file
            videoSize = pCodecCtx->width * pCodecCtx->height;
            fwrite(pAVFrameYuv->data[0], 1, videoSize, pFileSaveYuv);//y
            fwrite(pAVFrameYuv->data[1], 1, videoSize / 4, pFileSaveYuv);//u
            fwrite(pAVFrameYuv->data[2], 1, videoSize / 4, pFileSaveYuv);//v
        }

        //push the YUV data to render
        ret = m_SDLRender.render((void *const *)pAVFrameYuv->data, pAVFrameYuv->linesize);

        if (ret == -1)
        {
            printf("render video exit return \n");
            return -1;
        }

    }
    return 0;

}
int VideoDecoder::video_fini()
{
    m_SDLRender.fini();

    //video
    sws_freeContext(pSwsContext);
#ifdef OUTPUT_YUV420P
    if (pFileSaveYuv)
    {
        fclose(pFileSaveYuv);
        pFileSaveYuv = NULL;
    }
#endif
    av_frame_free(&pAVFrame);
    av_frame_free(&pAVFrameYuv);
    avcodec_close(pCodecCtx);
    return 0;
}
