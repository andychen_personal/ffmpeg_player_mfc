#pragma once
#include "CodecUtils.h"
extern "C"
{
#include "sdl2/SDL.h"
};
class AudioRender
{
private:
    SDL_Thread *pAudioRenderThread = NULL;
public:
    AudioRender();
    ~AudioRender();

    int audio_render_init(MediaCodecParams* pMediaParams);
    int audio_render(uint8_t *outBuffer, int bufferSize);
    int audio_fini();
};

