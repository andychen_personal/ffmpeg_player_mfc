#pragma once
#include "CodecUtils.h"

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"  
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <sdl2/SDL.h>
#ifdef __cplusplus
};
#endif
#endif



class AudioDecoder
{
private:
    AVCodecContext *pAudioCodecCtx = NULL;
    AVCodec *pAudioCodec = NULL;
    AVPacket *m_pAudioInputPacket;//avpkt The input AVPacket containing the input buffer.
    uint8_t *m_audio_out_buffer;
    int m_audio_out_buffer_size;
    AVFrame *m_pAudioAVFrame = NULL;
    FILE *m_pOutputPcmFile = NULL;
    int m_AudioDecInited = 0;
    SwrContext *m_pSwrCtx;
    
    
public:
    AudioDecoder();
    ~AudioDecoder();

    int audio_init(MediaCodecParams* pParams);
    int audio_decoder(int decStatus);
    int audio_fini();
    int m_decoder();
};

