#pragma once
#include "CodecUtils.h"



class VideoDecoder
{
public:
    VideoDecoder();
    ~VideoDecoder();
    int video_init(MediaCodecParams* pParams);
    int video_decoder();
    int video_fini();
};

