#pragma once
#include "CodecUtils.h"
#ifdef _WIN32
//Windows
extern "C"
{
#include "sdl2/SDL.h"
};
#else
//Linux...
#ifdef __cplusplus
extern "C"
{
#endif
#include "sdl2/SDL.h"
#ifdef __cplusplus
};
#endif
#endif



class VideoSdlRender
{
private:
    SDL_Window *pSdlWindown = NULL;
    SDL_Renderer *pSdlRender = NULL;
    SDL_Texture *pSdlTexture = NULL;
    SDL_Thread *pSdlThread = NULL;
    SDL_Rect srcRect,destRect;
    bool m_isInited = false;
    int screen_w, screen_h;
    int init_audio(MediaCodecParams * pMediaParams);
    
public:
    VideoSdlRender();
    ~VideoSdlRender();
    int init(MediaCodecParams * pMediaParams);
    int render(void* const yuvData[], int* linesize);
    int render_audio(uint8_t* outBuffer, int bufferSize);
    int fini();
};

